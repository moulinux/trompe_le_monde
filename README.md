# Trompe le monde

Voici une démonstration d'une mise en œuvre d'un déploiement automatisé
de code HTML via l'intégration continue de GitLab-CI.

Chaque nouveau commit, poussé sur la branche *master*, enclenche la publication
du code HTML :

* sur les [Framagit pages](https://moulinux.frama.io/trompe_le_monde/)
* sur un [serveur privé](http://bonjour.moulinux.net/), via le protocole
sftp.

> La mise en œuvre du protocole sftp par le logiciel *lftp* repose, assez
logiquement, sur ssh (d'où la nécessite d'installer le paquet `openssh-client`).
La création du fichier `~/.ssh/known_hosts`, à partir des variables de GitLab-CI,
permet d'éviter que le système demande, via un shell interactif, si l'on
souhaite bien enregistrer la clé de la machine distante à laquelle on se
connecte.
